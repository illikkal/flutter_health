import 'package:Health/widgets/feeling_item.dart';
import 'package:Health/widgets/top_eam_iem.dart';
import 'package:flutter/material.dart';

import '../widgets/list_tile_builder.dart';

class EmployeesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 4,
        ),
        padding: EdgeInsets.all(10),
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Theme.of(context).backgroundColor,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'All Employees',
              style: Theme.of(context).textTheme.headline1,
            ),
            ListTileBuilder(
              color: Color(0xff343593),
              icon: Icon(
                Icons.directions_walk,
                color: Colors.white,
              ),
              title: 'Total Steps',
              value: '23000',
            ),
            ListTileBuilder(
                color: Color(0xff51C0F2),
                icon: Icon(
                  Icons.whatshot,
                  color: Colors.white,
                ),
                title: 'Total Calories',
                value: '6.0k'),
            ListTileBuilder(
              color: Color(0xffE94258),
              icon: Icon(
                Icons.track_changes,
                color: Colors.white,
              ),
              title: 'Achieved Targets',
              value: '6.0',
            ),
            Container(
              margin: const EdgeInsets.all(12.0),
              child: Text(
                'How you are feeling today?',
                style: Theme.of(context).textTheme.headline1,
              ),
            ),
            Container(
              margin: EdgeInsets.all(5),
              padding: EdgeInsets.all(10),
              width: double.infinity,
              height: 150,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  FeelingItem(
                    color: Colors.grey,
                    icon: Icons.tag_faces,
                    title: 'Happy',
                  ),
                  FeelingItem(
                    color: Colors.yellow,
                    icon: Icons.sentiment_satisfied,
                    title: 'Okay',
                  ),
                  FeelingItem(
                    color: Colors.grey,
                    icon: Icons.sentiment_dissatisfied,
                    title: 'Sad',
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Top Teams',
                style: Theme.of(context).textTheme.headline1,
              ),
            ),
            Container(
              margin: EdgeInsets.all(5),
              padding: EdgeInsets.all(10),
              width: double.infinity,
              height: 400,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Column(
                children: [
                  TopTeamItem(
                    title: '1st.Management Team',
                    points: 49600,
                  ),
                  TopTeamItem(
                    title: '2nd.Development Team',
                    points: 49600,
                  ),
                  TopTeamItem(
                    title: '3rd.Designing Team',
                    points: 49600,
                  ),
                  FlatButton(
                    onPressed: () {},
                    child: Text('See All >',style: TextStyle(
                      color: Color(0xffE94258),
                    ),),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
