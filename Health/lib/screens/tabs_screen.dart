import '../screens/employees_screen.dart';
import 'package:flutter/material.dart';

class TabsScreen extends StatefulWidget {
  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white10,
        elevation: 0,
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: CircleAvatar(
            backgroundColor: Colors.red,
          ),
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'MicroSoft Inc',
              style: Theme.of(context).textTheme.headline1,
            ),
            Text(
              'New York',
              style: Theme.of(context).textTheme.subtitle1,
            )
          ],
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.notifications_none),
            onPressed: () {},
            color: Colors.black,
          ),
          Container(
            margin: EdgeInsets.all(10),
            child: DropdownButton(
              items: null,
              onChanged: null,
              iconSize: 30,
            ),
          )
        ],
      ),
      body: EmployeesScreen(),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              color: Colors.black,
            ),
            title: Text(
              'Home',
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.search,
              color: Colors.black,
            ),
            title: Text(
              'Discover',
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.people_outline,
              color: Colors.black,
            ),
            title: Text(
              'Teams',
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.trending_up,
              color: Colors.black,
            ),
            title: Text(
              'Insights',
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.list,
              color: Colors.black,
            ),
            title: Text(
              'Diet Plan',
              style: Theme.of(context).textTheme.subtitle1,
              
            ),
          ),
        ],
      ),
    );
  }
}
