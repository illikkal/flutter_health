import 'package:flutter/material.dart';

class FeelingItem extends StatelessWidget {
  IconData icon;
  Color color;
  String title;
  FeelingItem({this.icon, this.color, this.title});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: (){},
          child: Container(
            padding: EdgeInsets.all(25),
            decoration: BoxDecoration(
              color: Theme.of(context).backgroundColor,
              borderRadius: BorderRadius.circular(15)
            ),
            child: Icon(
              icon,
              color: color,
              size: 50,
            ),
          ),
        ),
        Text(
          title,
          style: Theme.of(context).textTheme.headline6,
        ),
      ],
    );
  }
}
