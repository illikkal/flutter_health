import 'package:flutter/material.dart';

class TopTeamItem extends StatelessWidget {
  final String title;
  final int points;

  TopTeamItem({this.title, this.points});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.circular(10),
      ),
      child: ListTile(
        title: Text(
          title,
          style: Theme.of(context).textTheme.headline2,
        ),
        subtitle: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              points.toString(),
              style: Theme.of(context).textTheme.subtitle1,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text('Points'),
            ),
          ],
        ),
        trailing: Icon(Icons.timelapse),
      ),
    );
  }
}
