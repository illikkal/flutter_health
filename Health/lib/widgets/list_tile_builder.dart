import 'package:flutter/material.dart';

class ListTileBuilder extends StatelessWidget {
  Color color;
  String title;
  Icon icon;
  String value;
  ListTileBuilder({
    this.color,
    this.icon,
    this.title,
    this.value
  });
  
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: 5,
        horizontal: 4,
      ),
      height: 100,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              color.withOpacity(0.5),
              color,
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
          borderRadius: BorderRadius.circular(15)),
      child: ListTile(
        leading: Container(
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: color,
          ),
          child: icon,
        ),
        title: Text(title,style: TextStyle(color: Colors.white,fontSize: 18),),
        trailing: Text(value,style: TextStyle(color: Colors.white,fontSize: 18),),
      ),
    );
  }
}